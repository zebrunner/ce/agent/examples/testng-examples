package util;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverUtils {

    public static RemoteWebDriver getDriver(String sessionName) throws MalformedURLException {
        ChromeOptions capabilities = new ChromeOptions();
        capabilities.addArguments("['start-maximized']");
        capabilities.setCapability("enableVNC", true);
        capabilities.setCapability("enableVideo", true);
        capabilities.setCapability("enableMetadata", true);
        capabilities.setCapability("enableLog", true);
        capabilities.setCapability("sessionName", sessionName);

        String seleniumUrl = System.getenv("SELENIUM_URL");
        if (seleniumUrl == null || seleniumUrl.isBlank()) {
            seleniumUrl = "https://stage:sIaSgFjWHTj5T4I2@hub.qaprosoft.farm/wd/hub";
        }
        System.out.println("Selenium URl for tests: " + seleniumUrl);
        RemoteWebDriver driver = new RemoteWebDriver(new URL(seleniumUrl), capabilities);

        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        return driver;
    }

    public static void doSomeDriverWork(RemoteWebDriver driver) throws InterruptedException {
        driver.get("https://www.freecrm.com/index.html");
        String title = driver.getTitle();

        DriverUtils.getRunTimeInfoMessage(driver, "info", title);

        if (title.equals("Free CRM software in the cloud powers sales and customer serviceQQQQ")) {
            DriverUtils.getRunTimeInfoMessage(driver, "info", "title is correct!! YAY!!!");
            Assert.assertTrue(true);
        } else {
            DriverUtils.getRunTimeInfoMessage(driver, "error", "title is not correct!! BUG BUG BUG!!!");
            ScreenshotUtils.takeScreenshot(driver);
        }
    }

    public static CallbackStatus doSomeDriverWorkAndReturnStatus(RemoteWebDriver driver) {
        try {
            DriverUtils.doSomeDriverWork(driver);
            return CallbackStatus.DONE;
        } catch (Exception e) {
            return CallbackStatus.UNDONE;
        }
    }

    private static void getRunTimeInfoMessage(RemoteWebDriver driver, String messageType, String message) throws InterruptedException {
        driver.executeScript("if (!window.jQuery) {"
                + "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
                + "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
                + "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
        Thread.sleep(5000);

        driver.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");

        driver.executeScript("$('head').append('<link rel=\"stylesheet\" "
                + "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
        Thread.sleep(5000);

        driver.executeScript("$.growl({ title: 'GET', message: '/' });");

        switch (messageType) {
            case "error":
                driver.executeScript("$.growl.error({ title: 'ERROR', message: '" + message + "' });");
                break;
            case "info":
                driver.executeScript("$.growl.notice({ title: 'Notice', message: '" + message + "' });");
                break;
            case "warning":
                driver.executeScript("$.growl.warning({ title: 'Warning!', message: '" + message + "' });");
                break;
        }
    }

}
