package driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import util.DriverUtils;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;

public class DriverOnlyBeforeAllTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @BeforeClass
    public static void setUp() throws MalformedURLException, InterruptedException {
        LOGGER.info("start setUp");
        RemoteWebDriver driver = DriverUtils.getDriver("@BeforeClass in DriverOnlyBeforeAllTest");
        DriverUtils.doSomeDriverWork(driver);
        driver.quit();
        LOGGER.info("end setUp");
    }

    @Test
    public void driver_TrackSession_WhenOnlyBeforeClass_0Sessions() {
    }

}
