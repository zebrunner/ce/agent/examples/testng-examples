package driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import util.DriverUtils;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;

public class FreeCrmTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Test
    public void driver_FreeCrmPageTest_1Session() throws InterruptedException, MalformedURLException {
        LOGGER.info("start test");
        RemoteWebDriver driver = DriverUtils.getDriver("FreeCrmTest#driver_FreeCrmPageTest()");
        DriverUtils.doSomeDriverWork(driver);
        LOGGER.info("end test");
    }

}
