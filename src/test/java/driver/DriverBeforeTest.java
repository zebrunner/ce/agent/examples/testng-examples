package driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.DriverUtils;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;

public class DriverBeforeTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private RemoteWebDriver driver;

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        LOGGER.info("start setUp");
        driver = DriverUtils.getDriver("@BeforeMethod in DriverBeforeTest");
        LOGGER.info("end setUp");
    }

    @Test
    public void driver_TrackSession_WhenBeforeDriverWorked_1Session() throws InterruptedException {
        LOGGER.info("start test");
        DriverUtils.doSomeDriverWork(driver);
        LOGGER.info("end test");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
