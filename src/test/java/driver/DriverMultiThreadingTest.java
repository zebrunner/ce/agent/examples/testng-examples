package driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import util.CallbackStatus;
import util.DriverUtils;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class DriverMultiThreadingTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final int THREADS_NUMBER = 2;

    private RemoteWebDriver driver;

    @Test
    public void driver_TrackSession_WhenDriverWorksInDifferentThreads_1Session() throws ExecutionException, InterruptedException, TimeoutException {
        LOGGER.info("start test");
        ExecutorService executorService = Executors.newFixedThreadPool(THREADS_NUMBER);

        driver = executorService.submit(() -> DriverUtils.getDriver("Separate thread in DriverMultiThreadingTest"))
                                .get();

        Future<CallbackStatus> firstWorkingResult = executorService
                .submit(() -> DriverUtils.doSomeDriverWorkAndReturnStatus(driver));
        Future<CallbackStatus> secondWorkingResult = executorService
                .submit(() -> DriverUtils.doSomeDriverWorkAndReturnStatus(driver));

        firstWorkingResult.get(2, TimeUnit.MINUTES);
        secondWorkingResult.get(2, TimeUnit.MINUTES);
        LOGGER.info("end test");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
