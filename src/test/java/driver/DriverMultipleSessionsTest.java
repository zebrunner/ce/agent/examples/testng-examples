package driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.DriverUtils;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;

public class DriverMultipleSessionsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static RemoteWebDriver firstDriver;
    private static RemoteWebDriver secondDriver;
    private RemoteWebDriver thirdDriver;

    @BeforeClass
    public static void setUpClass() throws MalformedURLException {
        LOGGER.info("start setUpClass");
        firstDriver = DriverUtils.getDriver("First driver from DriverMultipleSessionsTest in @BeforeClass");
        secondDriver = DriverUtils.getDriver("Second driver from DriverMultipleSessionsTest in @BeforeClass");
        LOGGER.info("end setUpClass");
    }

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        LOGGER.info("start setUpClass");
        thirdDriver = DriverUtils.getDriver("Third driver from DriverMultipleSessionsTest in @BeforeMethod");
        LOGGER.info("end setUpClass");
    }

    @Test
    public void driver_TrackSession_WhenUseMultipleInstances_firstTest_3Sessions() throws InterruptedException {
        LOGGER.info("start test");
        DriverUtils.doSomeDriverWork(firstDriver);
        LOGGER.info("end test");
    }

    @Test
    public void driver_TrackSession_WhenUseMultipleInstances_secondTest_3Sessions() throws InterruptedException {
        LOGGER.info("start test");
        DriverUtils.doSomeDriverWork(secondDriver);
        LOGGER.info("end test");
    }

    @Test
    public void driver_TrackSession_WhenUseMultipleInstances_thirdTest_3Sessions() throws InterruptedException {
        LOGGER.info("start test");
        DriverUtils.doSomeDriverWork(thirdDriver);
        LOGGER.info("end test");
    }

    @AfterMethod
    public void tearDown() {
        thirdDriver.quit();
    }

    @AfterClass
    public static void tearDownClass() {
        LOGGER.info("start afterClass");
        secondDriver.quit();
        try {
            firstDriver.quit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            firstDriver.quit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info("end afterClass");
    }

}
