package driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.DriverUtils;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;

public class DriverOnlyBeforeEachTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @BeforeMethod
    public void setUp() throws MalformedURLException, InterruptedException {
        LOGGER.info("start setUp");
        RemoteWebDriver driver = DriverUtils.getDriver("@BeforeMethod in DriverOnlyBeforeEachTest");
        DriverUtils.doSomeDriverWork(driver);
        driver.quit();
        LOGGER.info("end setUp");
    }

    @Test
    public void driver_TrackSession_WhenOnlyBefore_1Sessions() {
    }

}
