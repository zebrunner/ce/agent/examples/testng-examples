package driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import util.DriverUtils;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;

public class DriverBeforeClassTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static RemoteWebDriver driver;

    @BeforeClass
    public static void setUp() throws MalformedURLException, InterruptedException {
        LOGGER.info("start setUp");
        driver = DriverUtils.getDriver("@BeforeClass in DriverBeforeClassTest");
        DriverUtils.doSomeDriverWork(driver);
        LOGGER.info("end setUp");
    }

    @Test
    public void driver_TrackSession_WhenBeforeClassDriverWorked_1Session() throws InterruptedException {
        LOGGER.info("start test");
        DriverUtils.doSomeDriverWork(driver);
        LOGGER.info("end test");
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

}
